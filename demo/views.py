from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login

from .forms import LoginForm

def home(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect("/profile/")
        else:
            template_name = 'loginfailed.html'
            return render(request, template_name)
    else:
        form = {'form': LoginForm()}
        template_name = 'index.html'
    return render(request, template_name, form)

@login_required(login_url='/')
def profile(request):
    return render(request, "profile.html")
